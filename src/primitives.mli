open Michelson

val primitives : (string * (int * (Type.t -> Opcode.t list -> Opcode.t list))) list
